require "jora/version"
require "dry/monads"
require "dry/types"
require "jora/constants"
require "jora/schemas"
require "jora/operations/create_table_operation"
require "jora/operations/create_position_operation"
require "jora/player"
require "jora/table"
require "jora/state"
require "tty-prompt"

module Jora
  class Error < StandardError; end
  # Your code goes here...

  class Prompt
    include Dry::Monads[:result]

    attr_accessor :prompt, :state

    def initialize
      @prompt = TTY::Prompt.new
      start
    end

    def start
      system("clear") || system("cls") # should work on windows
      message = ["Welcome to this amazing little gem.", 
                  "A board the size of 5x5 by default has been created.",
                  "You can begin by using the PLACE command",
                  "You can also exit by typing EXIT or ctrl+C",
                  "--------------COMMANDS------------------",
                  "PLACE X,Y,DIRECTION",
                  "MOVE",
                  "LEFT",
                  "RIGHT",
                  "REPORT",
                  "EXIT", 
                  "\n"]

      command = prompt.ask(message.join("\n"))
      handle_commands(command)
    end

    def handle_commands(command)
      # recursive function which will continuously prompt the user
      command = command.nil? ? "" : command.upcase
      matcher = command.split(" ", 2).map(&:strip)

      case [matcher[0], matcher[1] || ""]
      in ["EXIT", _]      then exit
      in ["PLACE", str]   then handle_place_command(str)
      in _ if @state.nil? then puts "Cannot do anything until you declare the initial place."
      in ["REPORT", _]    then handle_report
      in [cmd, _]         then @state.on_command(cmd.upcase)
      in _                then #do nothing
      end

      new_command = prompt.ask("")
      handle_commands(new_command)
    end

    private

    def initialize_state(params)
      player = Jora::Player.new(params)
      table  = Jora::Table.new(width: 5, height: 5)
      init   = Jora::State.new.(player, table)
      @state = init.success? ? init.value! : nil
    end

    def sanitize_place(str)
      str = "" if str.nil?

      case str.split(",").map(&:strip)
      in [x, y, f] then Jora::Operations::CreatePositionOperation.new(x: x, y: y, facing: f).perform
      in _         then Failure(:invalid_options)
      end 
    end

    def handle_place_command(string)
      case [@state.nil?, sanitize_place(string)]
      in [false, arg] if arg.success? then @state.on_command(PLACE, arg.value!)
      in [true, arg]  if arg.success? then initialize_state(arg.value!)
      in _                            then # do nothing 
      end
    end

    def handle_report
      puts @state.on_command(REPORT) 
    end
  end
end
