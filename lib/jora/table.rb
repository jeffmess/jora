module Jora
  class Table
    attr_accessor :width, :height

    def initialize(options = {})
      @width  = options[:width] || 5 # default to 5
      @height = options[:height] || 5
    end

    def is_valid_position?(position)
      case position
      in {x: x, y: y} if x < 0 || y < 0            then false
      in {x: x, y: y} if x > @width || y > @height then false
      in _                                         then true
      end
    end
  end
end
