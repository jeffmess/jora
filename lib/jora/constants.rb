module Jora
  # COMPASS
  NORTH   = "NORTH"
  SOUTH   = "SOUTH"
  EAST    = "EAST"
  WEST    = "WEST"

  points  = [NORTH, EAST, SOUTH, WEST]
  COMPASS = {}
  points.each_with_index do |point, idx|
    right                  = idx+1 == points.length ? 0 : idx+1
    COMPASS[point]         = {}
    COMPASS[point][:left]  = points[idx-1]
    COMPASS[point][:right] = points[right]
  end

  # COMMANDS
  LEFT   = "LEFT"
  MOVE   = "MOVE"
  PLACE  = "PLACE"
  REPORT = "REPORT"
  RIGHT  = "RIGHT"
end
