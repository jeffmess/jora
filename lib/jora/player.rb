module Jora
  class Player

    attr_accessor :x, :y, :facing

    def initialize(options = {})
      @x      = options[:x] || 0
      @y      = options[:y] || 0 
      @facing = options[:facing] || NORTH
    end

    def turn(direction)
      @facing = calculate_turn(direction)
      @facing
    end

    def next_move
      # returns the coordinates of the next move.
      case @facing
      in NORTH then {x: @x, y: @y + 1}
      in EAST  then {x: @x + 1, y: @y}
      in SOUTH then {x: @x, y: @y - 1}
      in WEST  then {x: @x - 1, y: @y}
      end
    end

    def place(position)
      @x, @y, @facing = [position[:x], position[:y], position[:facing]]
    end

    def move_to(coords)
      position          = coords
      position[:facing] = @facing
      place position
    end

    def deconstruct
      [@x, @y]
    end
  
    def deconstruct_key
      {x: @x, y: @y, facing: @facing}
    end

    def to_s
      "#{@x}, #{@y}, #{@facing}"
    end

    private

    def calculate_turn(direction)
      case direction
      in :left  then COMPASS[@facing][:left]
      in :right then COMPASS[@facing][:right]
      in _      then @facing
      end
    end

  end
end
