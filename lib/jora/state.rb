require "dry/monads"

module Jora
  class State
    include Dry::Monads[:result]

    attr_accessor :player, :table

    # command handler
    def on_command(command, args={})
      case [command, args] 
      in [MOVE, _]   if @table.is_valid_position?(@player.next_move)
        @player.move_to(@player.next_move)
      in [PLACE, {x: x, y: y, facing: f}] if @table.is_valid_position?({x: x, y: y})
        @player.place(args)
      in [LEFT, _]   then @player.turn(:left)  
      in [RIGHT, _]  then @player.turn(:right)
      in [REPORT, _] then @player.to_s
      in _           then # dont do anything
      end
    end

    def call(player, table)
      if table.is_valid_position?(player.deconstruct_key)
        @player = player
        @table  = table
        Success(self)
      else
        Failure(:invalid_player_position)
      end
    end
  end
end
