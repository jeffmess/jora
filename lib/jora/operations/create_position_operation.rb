module Jora
  module Operations
    class CreatePositionOperation
      include Dry::Monads[:result]

      def initialize(params)
        @params = PositionSchema.call(params)
      end

      def perform
        @params.to_monad
      end
    end
  end
end
