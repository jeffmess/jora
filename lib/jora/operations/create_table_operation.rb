module Jora
  module Operations
    class CreateTableOperation
      include Dry::Monads[:result]

      def initialize(params)
        @params = TableSchema.call(params)
      end

      def perform
        case @params.to_monad
        in Success
          Success(Table.new(@params.to_h))
        in Failure  
          Failure(:invalid_params)
        end
      end
    end
  end
end
