require 'dry-schema'

Dry::Schema.load_extensions(:monads)

module Jora
  TableSchema = Dry::Schema.Params do
    required(:width).filled(:integer, gt?: 0)
    required(:height).filled(:integer, gt?: 0)
  end

  PositionSchema = Dry::Schema.Params do
    required(:x).filled(:integer)
    required(:y).filled(:integer)
    required(:facing).filled(included_in?: %w(NORTH EAST SOUTH WEST))
  end
end
