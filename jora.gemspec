require_relative 'lib/jora/version'

Gem::Specification.new do |spec|
  spec.name          = "jora"
  spec.version       = Jora::VERSION
  spec.authors       = ["Jeffrey van Aswegen"]
  spec.email         = ["jeffmess@gmail.com"]

  spec.summary       = %q{Code Challenge of a Robot on a Table}
  # spec.homepage      = "http://github.com"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.7.1")

  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  # spec.metadata["homepage_uri"] = spec.homepage
  # spec.metadata["source_code_uri"] = "TODO: Put your gem's public repo URL here."
  # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "tty-prompt"
  spec.add_dependency "dry-monads"
  spec.add_dependency "dry-types"
  spec.add_dependency "dry-schema"
end
