RSpec.describe Jora::Table do
  before(:all) do
    @table = Jora::Table.new({width: 5, height: 5})
  end

  it "should return true for 3,3 position" do
    expect(@table.is_valid_position?({x: 3, y: 3})).to eql(true) 
  end

  it "should return true for is_valid_position 0,0" do
    expect(@table.is_valid_position?({x: 0,y: 0})).to eql(true) 
  end

  it "should return true for is_valid_position 5,5" do
    expect(@table.is_valid_position?({x: 5,y: 5})).to eql(true) 
  end

  it "should return false for is_valid_position 6,5" do
    expect(@table.is_valid_position?({x: 6,y: 5})).to eql(false) 
  end

  it "should return false for is_valid_position 5,6" do
    expect(@table.is_valid_position?({x: 5, y: 6})).to eql(false) 
  end

  it "should return false for is_valid_position -1,5" do
    expect(@table.is_valid_position?(x: -1,y: 5)).to eql(false) 
  end

  context "CreateTableOperation" do
    it "should return a failure if the width is not an integer" do
      table = Jora::Operations::CreateTableOperation.new(height: 5, width: "5d").perform
      expect(table.failure?).to eql(true)
    end

    it "should return a successful table with valid params" do
      table = Jora::Operations::CreateTableOperation.new(height: 5, width: "3").perform
      expect(table.success?).to eql(true)
      value = table.value!
      expect(value.width).to eql(3)
      expect(value.height).to eql(5)
    end
  end
end
