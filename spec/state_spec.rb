require 'dry/monads/result'

RSpec.describe Jora::State do
  context "invalid player position" do
    it "should return a failure when trying to instantiate an invalid state" do
      table  = Jora::Table.new(width: 5, height: 5)
      player = Jora::Player.new(x: 6,y: 1, facing: Jora::NORTH)
      state  = Jora::State.new.(player, table)

      expect(state.failure?).to eql(true)
      expect(state.to_s).to eql("Failure(:invalid_player_position)")
    end
  end

  context "valid player position" do
    before(:all) do
      @table  = Jora::Table.new(width: 5, height: 5)
      @player = Jora::Player.new(x: 1, y: 2, facing: Jora::EAST)
      @state  = Jora::State.new.(@player, @table)
      @valid  = @state.value!
    end

    it "should instantiate successfully" do
      expect(@state.success?).to eql(true)
      expect(@state.value!.player).to eql(@player)
      expect(@state.value!.table).to eql(@table)
    end

    it "should move successfully" do
      @valid.on_command(Jora::MOVE)
      expect(@valid.player.deconstruct).to eql([2, 2])
      @valid.on_command(Jora::MOVE)
      expect(@valid.player.deconstruct).to eql([3, 2])
      @valid.on_command(Jora::LEFT)
      expect(@valid.player.facing).to eql(Jora::NORTH)
      @valid.on_command(Jora::MOVE)
      expect(@valid.player.deconstruct).to eql([3, 3])
      expect(@valid.on_command(Jora::REPORT)).to eql("3, 3, NORTH")
    end

    it "should be able to place the robot anywhere on the table" do
      position = Jora::Operations::CreatePositionOperation.new(x: 5, y: 5, facing: Jora::SOUTH).perform
      @valid.on_command(Jora::PLACE, position.value!.to_h)
      expect(@valid.player.deconstruct_key).to eql({x: 5, y: 5, facing: Jora::SOUTH})
    end

    it "should return the last valid location when trying to place it out of bounds" do
      position = Jora::Operations::CreatePositionOperation.new(x: 6, y: 5, facing: Jora::SOUTH).perform
      @valid.on_command(Jora::PLACE, position.value!.to_h)
      expect(@valid.player.deconstruct_key).to eql({x: 5, y: 5, facing: Jora::SOUTH})
    end
  end
end
