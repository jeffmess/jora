RSpec.describe Jora::Table do
  context "Success" do
    before(:all) do
      @position = Jora::Operations::CreatePositionOperation.new({x: 5, y: 5, facing: Jora::NORTH}).perform
    end

    it "should be valid" do
      expect(@position.success?).to eql(true)
    end 

    it "should contain the specified attributed" do
      expect(@position.value![:x]).to eql(5)
      expect(@position.value![:y]).to eql(5)
      expect(@position.value![:facing]).to eql(Jora::NORTH)
    end
  end

  context "Failure" do
    it "should fail if the facing is not a valid direction" do
      position = Jora::Operations::CreatePositionOperation.new({x: 5, y: 5, facing: "EWEST"}).perform
      expect(position.failure?).to eql(true)
    end
  end
end
