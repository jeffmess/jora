RSpec.describe Jora::Player do
  before(:all) do
    @player = Jora::Player.new(x: 1, y: 2, facing: Jora::NORTH)
  end

  it "should face WEST when directed left" do
    expect(@player.turn(:left)).to eql(Jora::WEST) 
  end

  it "should face SOUTH when directed left" do
    expect(@player.turn(:left)).to eql(Jora::SOUTH) 
  end

  it "should move to position 3,4 when told to" do
    @player.move_to({x: 3,y: 4})
    expect(@player.deconstruct_key).to eql({x: 3,y: 4,facing: Jora::SOUTH}) 
  end

  it "should remain facing SOUTH if not directed left or right" do
    expect(@player.turn("leftt")).to eql(Jora::SOUTH) 
  end

  it "should return [3, 3] as it's next position" do
    expect(@player.next_move).to eql({x: 3, y: 3}) 
  end

  it "should return [2, 4] as it's next position when turned right" do
    @player.turn(:right)
    expect(@player.next_move).to eql({x: 2,y: 4}) 
  end
end
